<?php
//https://github.com/ashwinks/PHP-LinkedIn-SDK
//http://localhost/linkedin_proj/auth_callback.php

/*
1. Connect with Linkedin
2. Enter keywords
3. Enter comment
4. It should search Linkedin profile and pages and auto-post comment (with link of profiles/pages that was commented)
*/
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<?php
if (isset($_POST['submit'])) {
	$content = $_POST['content'];
	$url = $_POST['url'];
	
	$_SESSION['content'] = $content;
	$_SESSION['url'] = $url;
}
?>

<?php
	require_once 'LinkedIn.php';

	$li = new LinkedIn(
	  array(
		'api_key' => '78cex3yg9sl12r', 
		'api_secret' => 'ewk6mbMxktCf0L2c', 
		'callback_url' => 'http://ec2-35-160-18-19.us-west-2.compute.amazonaws.com/linkedin_app/auth_callback.php'
	  )
	);


	$url = $li->getLoginUrl(
	  array(
		LinkedIn::SCOPE_BASIC_PROFILE, 
		LinkedIn::SCOPE_WRITE_SHARE,
		LinkedIn::LIN_COMPANY_ADMIN
	  )
	);

	echo 'Content to be shared:'  . '<br />';
	echo 'Content:' . $_SESSION['content'] . '<br />';
	echo 'URL:' . $_SESSION['url'] . '<br />';
	echo '<a href="' . $url . '">Share</a>';

	if (isset($_REQUEST['code'])) {	//we are redirected here
		$token = $li->getAccessToken($_REQUEST['code']);
		$token_expires = $li->getAccessTokenExpiration();
		
		//echo "Token = " . $token . '<br />';
		
		$postParams = array(
			'content' => array(
				'description' => $_SESSION['content'],
				'submitted-url' => $_SESSION['url']
			),
			'visibility' => array(
				'code' => 'anyone'
			)
		);

		$response = $li->post('/people/~/shares?format=json', $postParams);

		//var_dump($response);
	}
?>

<br />
<br />
<a href="add_message.php">Share something else</a>